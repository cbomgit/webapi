function parseUrl(req) {
    var ret = [];
    var urlTokens = req.url.split("?");
    if(urlTokens.length > 1) {
        var queryString = urlTokens[1];
        var queries = queryString.split("&");
        for(var i = 0; i < queries.length; i++) {
            var queryTokens = queries[i].split("=");
            var queryObj = {}
            queryObj[queryTokens[0]] = queryTokens[1];
            ret.push(queryObj);
        }
    } else {
        ret.push("No query parameters were specified.")
    }

    return ret;
}

function buildRequestInfo(req) {
    //array to hold request info
    var requestInfo = [];

    //add the headers to request info array
    requestInfo.push({'headers':req.headers});

    //parse url params and add to requestInfo
    requestInfo.push({'urlQueryParams':parseUrl(req)});

    return requestInfo;
}

var express = require('express');
var app = express();

app.set('trust proxy', true);

app.get('/gets', function(req, res) {

    //send the response
    res.send(buildRequestInfo(req));
})

app.post('/posts', function(req, res) {

    var requestInfo = buildRequestInfo(req);
    var requestBody = {};
    requestBody['request body'] = req.body;
    requestInfo.push(requestBody);
    res.send(requestInfo);
})

app.put('/puts', function(req, res) {
    var requestInfo = buildRequestInfo(req);
    var requestBody = {};
    requestBody['request body'] = req.body;
    requestInfo.push(requestBody);
    res.send(requestInfo);
})

app.delete('/deletes', function(req, res) {
    //send the response
    res.send(buildRequestInfo(req))
})

app.listen(3000, function() {
   console.log('Listening on 3000!')
})


