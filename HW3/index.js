var express = require('express');
var request = require('request');
var app = express();

app.set('trust proxy', true);

app.get('/gituser/:username/', function(req, res) {

    var requestedUser = req.params['username'];
    request({
        uri: 'https://api.github.com/users/'+requestedUser,
        headers: {
            'User-Agent':'my-node-proxy',
            'Accept':'application/json',
            'Content-Type':'application/json'
        },

    }).pipe(res);
});

app.listen(3000, function() {
   console.log('Listening on 3000!')
})


