var express = require('express');
var request = require('request');
var router = express.Router();


/* GET all movies */
router.get('/movies', function(req, res, next) {

    request('https://apibaas-trial.apigee.net/cbomMoviesWebAPI4/sandbox/movies', function(error, response, body) {

        var entities = JSON.parse(body).entities;
        res.json({'status':'Ok', 'count':entities.length, 'movies':entities});    });

});

/* Get a single movie based on title */
router.get('/movies/:movieTitle', function(req, res, next) {

    var queryStr = "?ql=title='"+req.params['movieTitle']+"'";
    request('https://apibaas-trial.apigee.net/cbomMoviesWebAPI4/sandbox/movies' + queryStr, function(error, response, body) {
        if(JSON.parse(body).entities.length == 0){
            res.status(404);
            res.json({
                'error':{
                    'status':404,
                    'message':'Movie not found'
                }
            })
        }else {
            var entities = JSON.parse(body).entities;
            res.json({'status':'Ok', 'count':entities.length, 'movies':entities});
        }
    });

});

/*Create a new movie*/
router.post('/movies', function(req, res, next) {

    if(req.body['title'] == null ){
        res.status(400);
        res.json({
            'error':{
                'status':400,
                'message':'The title is missing'
            }
        });
    }
    else if(req.body['date_released'] == null ){
        res.status(400);
        res.json({
            'error':{
                'status':400,
                'message':'Date released is missing'
            }
        });
    }
    else if(req.body['actors'] == null) {
        res.status(400);
        res.json({
            'error': {
                'status':400,
                'message':'Actors array is required'
            }
        });
    }
    else if(req.body['actors'].length != 3) {
        res.status(400);
        res.json({
            'error': {
                'status':400,
                'message':'Provide at least 3 actors'
            }
        });
    }
    else {
        var options = {
            uri:'https://apibaas-trial.apigee.net/cbomMoviesWebAPI4/sandbox/movies',
            method:'POST',
            json: {
                "name":req.body['title'],
                "title":req.body['title'],
                "date_released":req.body['date_released'],
                "actors":req.body['actors']
            }
        };

        request(options, function(error, response, body) {
            res.json({'status':'Ok', 'movies':body.entities});
        });
    }
});

/*Delete a movie by name*/
router.delete('/movies/:name', function(req, res, next) {
    var options = {
        uri:'https://apibaas-trial.apigee.net/cbomMoviesWebAPI4/sandbox/movies/'+req.params['name'],
        method:'DELETE'
    };

    request(options, function(error, response, body) {
        if(!JSON.parse(body).entities){
            res.status(404);
            res.json({
                'error':{
                    'status':404,
                    'message':'Movie not found'
                }
            })
        }else {
            res.json({'status':'Ok', 'movies':JSON.parse(body).entities});
        }
    });
});

module.exports = router;
